package com.domosnap.microservice.engine;

import java.util.ArrayList;
import java.util.List;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/")
@Api(value = "House")
public interface HouseResource {

	// "Usage: http[s]://server:port/house\n"

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(
			value = "Returns house information", 
			notes = "Returns house information as title, description, etc... Plus list of groups and labels. Groups are the list of controllers physicaly organized. Labels are the list of controllers logically organized."
			)
	public House getHouse();

	@DELETE
	@ApiOperation(value = "Cleanup the house", notes = "Remove all labels from the house")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "OK (no content)") })
	public void deleteHouse();

	@ApiModel
	@XmlRootElement(name="house")
	@XmlAccessorType(XmlAccessType.FIELD)
	public class House {
		@XmlElement
		private String description;
		@XmlElement
		private String title;
		@XmlElement
		private List<String> groups = new ArrayList<String>();
		@XmlElement
		private List<String> labels = new ArrayList<String>();

		public House() {
		}

		@ApiModelProperty(example="House's description")
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@ApiModelProperty(example="Title's description")
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		@ApiModelProperty
		public List<String> getGroups() {
			return groups;
		}

		public void setGroups(List<String> groups) {
			this.groups = groups;
		}

		@ApiModelProperty
		public List<String> getLabels() {
			return labels;
		}

		public void setLabels(List<String> labels) {
			this.labels = labels;
		}
	}
}
