package com.domosnap.microservice.engine.recorder.http;

import java.net.URI;

/*
 * #%L
 * recorder-microservice-standalone
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.text.MessageFormat;
import java.util.List;

import com.domosnap.engine.Log;
import com.domosnap.engine.Log.Session;
import com.domosnap.engine.adapter.ControllerAdapter;
import com.domosnap.engine.adapter.core.Command;
import com.domosnap.engine.adapter.core.UnknownControllerListener;
import com.domosnap.engine.adapter.impl.hue.HueControllerAdapter;
import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerAdapter;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.event.EventFactory;

import io.vertx.core.AbstractVerticle;

public class RecorderMicroservice extends AbstractVerticle {

	
	private HttpLogAppender appender;
	private Log log = new Log(RecorderMicroservice.class.getName());
	
	@Override
	public void start() {
		log.debug = true;
		
		if (config().getBoolean("log.enable", true)) {
			appender = new HttpLogAppender();
			Log.addAppender(appender);
			if (log.debug) {
				log.finest(Session.Other, "Http log enabled!");
			}
		}
		
		ControllerAdapter service;
		String uriStr = config().getString("gateway.uri", "openwebnet://12345@localhost:1234");
		URI uri = URI.create(uriStr);
		
		switch (uri.getScheme()) {
		case "hue":
			service = new HueControllerAdapter(uriStr);
			break;
		case "openwebnet":			
		default:
			service = new OpenWebNetControllerAdapter(uriStr);
			break;
		}
		
		service.addUnknowControllerListener(new UnknownControllerListener() {
			@Override
			public void foundUnknownController(Command command) {
				List<What> whatList = command.getWhatList();
				for (What what : whatList) {
					String whereStr = command.getWhere() != null ? command.getWhere().getPath() : "null";
					String whatStr = what == null ? "null": what.getName();
					String valueStr = what == null ? "null": what.getValue() == null ? "null" : what.getValue().toString();
					log.info(Session.Other, MessageFormat.format("Who [{0}] : Where [{1}] : what [{2}] : value [{3}]\n", command.getWho(), whereStr, whatStr, valueStr));
					log.info(Session.Other, command.toString());
					EventFactory.SendEvent(Session.Monitor, command);
				}
			}
		});
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		appender.shutdown();
		appender = null;
	}

}
