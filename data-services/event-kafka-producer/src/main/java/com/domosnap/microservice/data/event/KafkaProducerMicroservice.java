package com.domosnap.microservice.data.event;

/*
 * #%L
 * event-kafka-producer
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;

import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.kafka.client.producer.KafkaProducer;
import io.vertx.rxjava.kafka.client.producer.KafkaProducerRecord;

public class KafkaProducerMicroservice extends AbstractVerticle{

	
	public String CONFIG_KAFKA_TOPIC = "default.kafka.topic";
	public String CONFIG_KAFKA_BOOTSTRAP_SERVERS_CONFIG = "kafka.bootstrap.servers.config";
	
	private KafkaProducer<String, String> producer;
	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	private String VERTX_TOPIC_LISTEN = "event-topic";
	private String DEFAULT_CONFIG_KAFKA_TOPIC_WRITE = "domosnap-event-topic";
	
	
	@Override
	public void start() {
		
		// TODO put in config!!!!
		Map<String, String> props = new HashMap<>();
		 props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, config().getString(CONFIG_KAFKA_BOOTSTRAP_SERVERS_CONFIG,"PLAINTEXT://localhost:9092"));
		 props.put(ProducerConfig.ACKS_CONFIG, "all");
		 props.put(ProducerConfig.RETRIES_CONFIG, "0");
		 props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100");
		 props.put(ProducerConfig.LINGER_MS_CONFIG, "1");
		 props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
		 props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
		 props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

		 producer = KafkaProducer.create(vertx, props, String.class, String.class);
		 producer.exceptionHandler(e-> {
			 logger.error(e.getMessage());
		 });
		 
		// Receive message from vertx bus
		vertx.eventBus().<String>consumer(VERTX_TOPIC_LISTEN, message -> {
			// Check whether we have received a payload in the
			// incoming message
			System.out.println(message.body().toString());
			if (message.body().isEmpty()) {
				logger.error("Message empty... Message removed.");
			} else {
				try {
					String channel =  config().getString(CONFIG_KAFKA_TOPIC, DEFAULT_CONFIG_KAFKA_TOPIC_WRITE);
					JsonParser jp = new JsonFactory().createParser(message.body());
					String uuid = null;
					 
					do {
						String name = jp.nextFieldName();
						if ("uuid".equals(name)) {
							uuid = jp.nextTextValue();
						}
						if ("channel".equals(name)) {
							channel = jp.nextTextValue();
						}
					} while(!jp.isClosed());

					if (uuid == null) {
						logger.error("Message without UUID... [" + message.body() + "]");
					} else {
						final String key = uuid;
						producer.write(KafkaProducerRecord.create(channel, uuid, message.body()), record -> {
							if (record.succeeded()) {
								logger.info("Message [" + key + "] envoyé.");	
							} else {
								logger.error(record.cause());	
							}
						});
						logger.info("Message [" + uuid + "] en cours d'envoie");
					}
				} catch (JsonParseException e1) {
					logger.error(e1.getMessage());
				} catch (IOException e1) {
					logger.error(e1.getMessage());
				}
			}
		});

	}

	@Override
	public void stop() throws Exception {
		producer.close();
		producer = null;
	}

	public static void main(String[] args) {
		io.vertx.core.Vertx vertx = io.vertx.core.Vertx.vertx();
		vertx.deployVerticle(KafkaProducerMicroservice.class.getName());
		
	}	
}
