package com.domosnap.microservice.engine;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

@Path("/groups")
@Api(value = "Group")
public interface GroupResource {

//	"http[s]://server:port/house/groups\n"
//	"http[s]://server:port/house/groups/{groupId}\n"
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(	
		value = "Get the list of groups",
		notes = "Return list of groups at the first level.",
		response = RestGroup.class,
		responseContainer = "List"
		)
	@ApiResponses(value = { 
		@ApiResponse(code = 200,
			message = "Return the list of groups"
		),
		@ApiResponse(code = 500,
			message = "Error while getting the list"
		)
	})
	public List<RestGroup> getGroups();

	@GET
	@Path("/{groupId}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the group", notes = "Return the group corresponding to the id")
	public RestGroup getGroup(@PathParam("groupId") String id);

	@PUT
	@Path("/{groupId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update title and description of a group.", notes = "")
	public RestGroup updateGroup(@PathParam("groupId") String id, @ApiParam(value = "Updated group object", required = true) RestGroup body);

	@ApiModel
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public class RestGroup {
		@XmlElement
		private String id;
		@XmlElement
		private String description;
		@XmlElement
		private String title;
		@XmlElement
		private List<String> controllers = new ArrayList<String>();

		public RestGroup() {
		}

		@ApiModelProperty(example="Group's description")
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@ApiModelProperty(example="Group's title")
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		@ApiModelProperty
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	
		@ApiModelProperty
		public List<String> getControllers() {
			return controllers;
		}

		public void setControllers(List<String> controllers) {
			this.controllers = controllers;
		}

	}
}
