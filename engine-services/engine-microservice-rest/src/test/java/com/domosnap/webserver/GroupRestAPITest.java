package com.domosnap.webserver;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.jboss.resteasy.util.HttpResponseCodes;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GroupRestAPITest extends AbstractRestApi {

	private static final String urn_groups = urn + "/groups";

//	@Before
//	public void init() {
//		try {
//			copyFileUsingFileChannels(new File("Backup of house.xml"), new File("house.xml"));
//		} catch (IOException e) {
//			Assert.fail("Impossible to initialize file");
//		}
//	}

	@Test
	public void test0DeleteGroupList() {
		JSONObject jo = deleteRequestJSONObject(urn_groups, HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
		Assert.assertNull(jo);

	}
	
	@Test
	public void test1CreateGroupList() {
		postRequestJSONObject(urn_groups+ "/6","{}", HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	@Test
	public void test2PutGroupList() {
		putRequestJSONObject(urn_groups,"{}", HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}
	
	@Test
	public void test4GetGroup() {
		// Test get all groups
		JSONArray groups = getRequestJSONArray(urn_groups);
		Assert.assertEquals(9, groups.length());

		testGroup10(groups.getJSONObject(1));
		
		// Test to get a specific group
		JSONObject group = getRequestJSONObject(urn_groups + "/1");
		// Test group 1
		Assert.assertNotNull(group);
		testGroup1(group);

		group = getRequestJSONObject(urn_groups + "/10");
		// Test group 10
		Assert.assertNotNull(group);
		testGroup10(group);
	}
	
	@Test
	public void test6ModifyGroup() {
		// Impossible to modify
		putRequestJSONObject(urn_groups + "/1", createGroup1Bis(), HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	
	@Test
	public void test11DeleteControllerByGroup() throws UnsupportedEncodingException {
		// Test impossible to create an existing controller
		deleteRequestJSONObject(urn_groups + "/1/" + URLEncoder.encode("openwebnet://12345@localhost:1234/15", "UTF-8"), HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	
	@Test
	public void test8DeleteGroup() {
		// Test to get a specific group
		deleteRequestJSONObject(urn_groups + "/10", HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	@Test
	public void test9DeleteGroupList() {
		// Test impossible to create grouplist
		deleteRequestJSONObject(urn_groups, HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	private void testGroup1(JSONObject group) {
		testGroup(group, "1", "Group 1", 5);
	}
	
	private String createGroup1Bis() {
		return createJsonGroup("Group Bis 1", "1", "");
	}
	
	private void testGroup10(JSONObject group) {
		testGroup(group, "10", "Group 10", 0);
	}
}
