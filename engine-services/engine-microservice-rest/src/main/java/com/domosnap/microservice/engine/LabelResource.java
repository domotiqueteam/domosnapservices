package com.domosnap.microservice.engine;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriBuilderException;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.cxf.jaxrs.ext.ResponseStatus;

import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.house.Label;
import com.domosnap.microservice.engine.tools.HouseRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

@Path("/labels")
@Api(value = "Label")
public interface LabelResource {

//	"http[s]://server:port/house/labels\n"
//	"http[s]://server:port/house/labels/{labelId}\n"
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(	
		value = "Get the list of labels",
		notes = "Return list of labels at the first level. Labels can contains sub label (not in first version): to get sub version you should call link.",
		response = RestLabel.class,
		responseContainer = "List"
		)
	@ApiResponses(value = { 
		@ApiResponse(code = 200,
			message = "Return the list of labels"
		),
		@ApiResponse(code = 500,
			message = "Error while getting the list"
		)
	})
	public List<RestLabel> getLabels();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ResponseStatus(value=Response.Status.CREATED)
	@ApiOperation(
			value = "Add a label", 
			notes = "Add a label to the list."
			)
	@ApiResponses(value = {
			@ApiResponse(code = 201,
				message = "Label have been created!. Return the label.",
				response = RestLabel.class
			),
			@ApiResponse(code = 400,
			message = "Provided ID is already existing."
			)
	})
	public Response addLabel(@ApiParam(value = "Updated label object", required = true) RestLabel body, @Context UriInfo uriInfo);

	@DELETE
	@ApiOperation(
			value = "Remove all labels", 
			notes = "Reset the list of labels: all labels will be lost!"
			)
	@ApiResponses(value = { 
			@ApiResponse(code = 204,
				message = "OK (no content) = List has been cleaned."
			)
	})
	public void deleteLabels();

	@GET
	@Path("/{labelId}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the label", notes = "Return the label corresponding to the id")
	public RestLabel getLabel(@PathParam("labelId") String id);

	@PUT
	@Path("/{labelId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update or create a label.", notes = "")
	public RestLabel updateLabel(@PathParam("labelId") String id, @ApiParam(value = "Updated label object", required = true) RestLabel body);

	@DELETE
	@Path("/{labelId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove the label from the list", notes = "Remove the label from the list")
	@ApiResponses(value = { 
			@ApiResponse(code = 204,
				message = "OK (no content)"
			)
	})
	public void deleteLabel(@PathParam("labelId") String id);
	
	@PUT
	@Path("/{labelId}/{controllerId}")
	@ApiOperation(value = "Add a controller to the label")
	@Produces(MediaType.APPLICATION_JSON)
	public RestLabel putControllerInLabel(@PathParam("labelId") String labelId, @PathParam("controllerId") String controllerId);

	
//	TODO create sub label...
//	@POST
//	@Path("/{id}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public String createLabelByPath(@PathParam("id") String id);
	
	@ApiModel
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public class RestLabel {
		@XmlElement
		private String id;
		@XmlElement
		private String description;
		@XmlElement
		private String title;
		@XmlElement
		private List<String> controllers = new ArrayList<String>();
		@XmlElement
		private List<String> labels = new ArrayList<String>();

		public RestLabel() {
		}

		@ApiModelProperty(example="Label's description")
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@ApiModelProperty(example="Label's title")
		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		@ApiModelProperty
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}
	
		@ApiModelProperty
		public List<String> getControllers() {
			return controllers;
		}

		public void setControllers(List<String> controllers) {
			this.controllers = controllers;
		}

		@ApiModelProperty
		public List<String> getLabels() {
			return labels;
		}

		public void setLabels(List<String> labels) {
			this.labels = labels;
		}

		public RestLabel mapFrom(Label engineLabel) {
			
			this.setDescription(engineLabel.getDescription());
			List<Controller> l = engineLabel.getControllerList();
			for (Controller controller : l) {
				try {
					this.getControllers().add(UriBuilder.fromResource(ControllerResource.class).path(URLEncoder.encode(controller.getWhere().getUri(), "UTF-8")).build().toString());
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UriBuilderException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
//			restLabel.setLabels(labels);
			this.setTitle(engineLabel.getTitle());
			this.setId(engineLabel.getId());
			return this;
		}
		
		public RestLabel mapTo(Label engineLabel) {
			
			engineLabel.setDescription(this.getDescription());
			List<String> l = this.getControllers();
			for (String controllerURI : l) {
				// TODO renforcer le code ici => s'assurer que c'est bien une URI
				String id = controllerURI.substring(controllerURI.lastIndexOf("/"), controllerURI.length());
				engineLabel.getControllerList().add(HouseRepository.getInstance().getController(id));
			}
//			restLabel.setLabels(labels);
			engineLabel.setTitle(this.getTitle());
			if (engineLabel.getId() == null) {
				engineLabel.setId(this.getId());
			}
			return this;
		}
	}
}
