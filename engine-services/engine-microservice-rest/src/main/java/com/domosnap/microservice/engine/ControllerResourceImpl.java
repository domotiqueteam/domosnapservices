package com.domosnap.microservice.engine;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/*
 * #%L
 * engine-microservice-rest
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import com.domosnap.engine.controller.Controller;
import com.domosnap.microservice.engine.tools.HouseRepository;

public class ControllerResourceImpl implements ControllerResource {

	private HouseRepository hr = HouseRepository.getInstance();

	@Override
	public List<RestController> getAllControllers() {
		List<RestController> rcs = new ArrayList<ControllerResource.RestController>();
		for (Controller c  : hr.getControllers()) {
			rcs.add(new RestController().mapFrom(c));
		}
		return rcs;
	}
	
	@Override
	public RestController getControllerById(String controllerId)  {
		try {
			return new RestController().mapFrom(hr.getController(URLDecoder.decode(controllerId, "UTF-8") ));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public RestController putControllerWithEntity(String id, RestController controller) {
		Controller c;
		try {
			c = hr.getController(URLDecoder.decode(id, "UTF-8") );

			controller.mapTo(c);
			// Return the upadted controller
			RestController rc = new RestController();
			return rc.mapFrom(c);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
