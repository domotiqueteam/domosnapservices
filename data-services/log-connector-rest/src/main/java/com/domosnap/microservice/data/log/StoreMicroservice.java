package com.domosnap.microservice.data.log;

/*
 * #%L
 * log-connector-rest
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import java.util.function.Consumer;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.AsyncSQLClient;
import io.vertx.ext.asyncsql.MySQLClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class StoreMicroservice extends AbstractVerticle {

	private AsyncSQLClient client;
	private boolean useStoreMessageService = false;
	private EventBus bus;
	
	@Override
	public void start() {
		
		Router router = Router.router(vertx);
		router.post("/").handler(this::store);
		vertx.createHttpServer().requestHandler(router::accept).listen(8080);
		
		JsonObject mySQLClientConfig = new JsonObject().put("host", "localhost");
		mySQLClientConfig.put("username", "arnaud");
		mySQLClientConfig.put("password", "arnaud");
		mySQLClientConfig.put("database", "homesnap");

		 client = MySQLClient.createShared(vertx, mySQLClientConfig);
		 bus = vertx.eventBus();
	}

	
	private void store(RoutingContext rc) {
		String ip = String.valueOf(rc.request().connection().remoteAddress()); 
		
		rc.request().bodyHandler(
				new Handler<Buffer>() {
					
					@Override
					public void handle(Buffer event) {
						event.toJsonArray().forEach(new Consumer<Object>() {

							@Override
							public void accept(Object t) {
								if (useStoreMessageService) {
									storeMessage(String.valueOf(t), ip);
								}
								
								storeInDB(String.valueOf(t), ip);
								System.out.println(String.valueOf(t)+ " : " + ip);
							}
						});
					}
				});
		rc.response().end();
	}
	
	private void storeInDB(String json, String ip) {
		String sql = "insert into log(ip,jdoc) value(?,?)";
		client.getConnection(res -> {
				  if (res.succeeded()) {
					// Got a connection  
				    SQLConnection connection = res.result();
				    connection.queryWithParams(sql, new JsonArray().add(ip).add(json), res2 -> {
				    	if (res2.succeeded()) {
				    		ResultSet result = res2.result();
				    		System.out.println(result.getNumRows());
				    	} else {
				    		// TODO failed
				    		System.out.println("Failed : " + res2.cause().getMessage());
				    	}
				    });
				    connection.close();
				  } else {
				    // TODO Failed to get connection - deal with it
					  System.out.println("Failed : " + res.cause().getMessage());
				  }
				});
	}
	
	
	private void storeMessage(String json, String ip) {
		bus.send("Message", "{\"json\"=" + json +", \"ip\":" +ip +"}");
	}
}
