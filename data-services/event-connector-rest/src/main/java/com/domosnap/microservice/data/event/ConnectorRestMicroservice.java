package com.domosnap.microservice.data.event;

/*
 * #%L
 * event-connector-rest
 * %%
 * Copyright (C) 2017 - 2018 A. de Giuli
 * %%
 * This file is part of HomeSnap done by Arnaud de Giuli (arnaud.degiuli(at)free.fr)
 *     helped by Olivier Driesbach (olivier.driesbach(at)gmail.com).
 * 
 *     HomeSnap is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HomeSnap is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with HomeSnap. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.AuthHandler;
import io.vertx.ext.web.handler.BasicAuthHandler;


public class ConnectorRestMicroservice extends AbstractVerticle {

	
	public String CONFIG_PORT = "port";
	
	private String CONFIG_VERTX_TOPIC_LISTEN = "event-topic";
	private String version = "1.0";
	private Logger logger = LoggerFactory.getLogger(this.getClass().getName());
	
	
	@Override
	public void start() {

		Router router = Router.router(vertx);
		
		AuthHandler basicAuthHandler = BasicAuthHandler.create(new AuthProvider() {
			@Override
			public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {
				
				resultHandler.handle(new AsyncResult<User>() {

					@Override
					public User result() {
						return null;
					}

					@Override
					public Throwable cause() {
						return null;
					}

					@Override
					public boolean succeeded() {
						return "domosnap".equals(authInfo.getString("username")) && "#h7EPj-zEm^Mu5eEAp6wpXc-_$ey6Y!m".equals(authInfo.getString("password"));
					}

					@Override
					public boolean failed() {
						return false;
					}
				});
				
			}
		});
		
		router.route("/*").handler(basicAuthHandler);
		
		
		router.post("/").handler(this::store);
		vertx.createHttpServer().requestHandler(router::accept).listen(config().getInteger(CONFIG_PORT,8080));
		
	}

	// TODO faire une doc swagger pour le service!
	private void store(RoutingContext rc) {
//		String ip = String.valueOf(rc.request().connection().remoteAddress());

		rc.request().bodyHandler(new Handler<Buffer>() {

			@Override
			public void handle(Buffer event) {
				
				// Check whether we have received a payload in the incoming message
				JsonObject result = new JsonObject().put("served-by", this.toString());
				result.put("version", version);
				String message;
				try {
					JsonObject json = event.toJsonObject();
					if (json.isEmpty()) {
						message = "Message was empty...";
						rc.response().setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
					} else {
					
						// TODO ADD a checksum?
	
						vertx.eventBus().<String>publisher(CONFIG_VERTX_TOPIC_LISTEN).send(json.toString());
						message = "Succes!";
						rc.response().setStatusCode(HttpResponseStatus.OK.code());
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					message = "error!";
				}
				result.put("message", message);
				rc.response().putHeader("Content-Length", "" + result.size());
				rc.response().end(result.toString());
			}
		});
	}

}
