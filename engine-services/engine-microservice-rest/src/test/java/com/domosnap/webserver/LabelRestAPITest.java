package com.domosnap.webserver;

/*
 * #%L
 * HomeSnapWebServer
 * %%
 * Copyright (C) 2011 - 2016 A. de Giuli
 * %%
 * This file is part of HomeSnap done by A. de Giuli (arnaud.degiuli(at)free.fr).
 * 
 *     MyDomo is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MyDomo is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with MyDomo.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.jboss.resteasy.util.HttpResponseCodes;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.domosnap.engine.controller.light.Light;
import com.domosnap.engine.controller.what.impl.OnOffState;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LabelRestAPITest extends AbstractRestApi {

	private static final String urn_labels = urn + "/labels";
	@Before
	public void init() {
		try {
			copyFileUsingFileChannels(new File("Backup of house.xml"), new File("house.xml"));
		} catch (IOException e) {
			Assert.fail("Impossible to initialize file");
		}
	}

	@Test
	public void test1DeleteLabelList() {
		JSONObject result = deleteRequestJSONObject(urn_labels, HttpResponseCodes.SC_NO_CONTENT);
		Assert.assertNull(result);

		// Test there is no more controllers
		JSONArray controllers = getRequestJSONArray(urn_labels);
		Assert.assertEquals(0, controllers.length());
	}
	
	@Test
	public void test2CreateLabelList() {
		// Test impossible to create label list
		postRequestJSONObject(urn_labels+ "/ch6","{}", HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	@Test
	public void test3CreateLabel() {
		// Test new label creation
		JSONObject label = postRequestJSONObject(urn_labels, createJsonLabelCh6(), HttpResponseCodes.SC_CREATED, urn_labels + "/ch6");
		testLabelCh6(label);
		
		label = postRequestJSONObject(urn_labels, createJsonLabelCh7(), HttpResponseCodes.SC_CREATED, urn_labels + "/ch7");
		testLabelCh7(label);
		
		label = postRequestJSONObject(urn_labels, createJsonLabelCh7(), HttpResponseCodes.SC_BAD_REQUEST);
		Assert.assertNull(label);
	}
	
	@Test
	public void test4GetLabelList() {
		// Test to get labels list
		JSONArray labels = getRequestJSONArray(urn_labels);

		// Test labels
		testLabelCh6(labels.getJSONObject(0));
		testLabelCh7(labels.getJSONObject(1));
	}

	@Test
	public void test5ModifyLabelList() {
		// Test impossible to modify label list
		putRequestJSONObject(urn_labels,"{}", HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
	}

	@Test
	public void test6GetLabel() {
		// Test to get a specific label
		JSONObject label = getRequestJSONObject(urn_labels + "/ch6");
		// Test label Ch1
		testLabelCh6(label);

		label = getRequestJSONObject(urn_labels + "/ch7");
		// Test label Ch1
		testLabelCh7(label);
	}

	@Test
	public void test7UpdateLabel() {
		// Test new label creation
		JSONObject label = putRequestJSONObject(urn_labels + "/ch6", createJsonLabelCh6Bis(), HttpResponseCodes.SC_OK);
		testLabelCh6Bis(label);
		
		label = putRequestJSONObject(urn_labels + "/ch7", createJsonLabelCh7Bis(), HttpResponseCodes.SC_OK);
		testLabelCh7Bis(label);
	}

	
	@Test
	public void test02CreateControllerFromLabel() throws UnsupportedEncodingException {
		// Test controller add in a label => forbidden!!! only creation by group is authorized
		JSONObject jo = postRequestJSONObject(urn_labels, createJsonLabel("Chambre 1", "ch1"), HttpResponseCodes.SC_CREATED);
		testLabel(jo, "ch1", "Chambre 1", 0);

		jo = postRequestJSONObject(urn_labels + "/ch1/" + URLEncoder.encode("openwebnet://12345@localhost:1234/11", "UTF-8"), createJsonController11(), HttpResponseCodes.SC_METHOD_NOT_ALLOWED);
		testController11(jo);
		
//		putRequestJSONObject(urn_labels + "/ch1/", , HttpResponseCodes.SC_NOT_FOUND);
		
//		postRequestJSONObject(urn_groups + "/1", createJsonController11(), HttpResponseCodes.SC_NOT_FOUND, urn_groups + "/1/11");
//		postRequestJSONObject(urn_groups + "/1", createJsonController12(), HttpResponseCodes.SC_NOT_FOUND, urn_groups + "/1/12");
		
//		JSONObject jo = postRequestJSONObject(urn_groups + "/1", createJsonController16(), HttpResponseCodes.SC_CREATED, urn_groups + "/1/16");
//		testController16(jo);
		// impossible to add again the same controller
//		postRequestJSONObject(urn_groups + "/1/16", createJsonController16(), HttpResponseCodes.SC_NOT_FOUND);

//		postRequestJSONObject(urn_groups + "/1", createJsonController17(), HttpResponseCodes.SC_NOT_FOUND, urn_groups + "/1/17");
//		testController17(jo);
	}
	
	// Delete
	@Test
	public void test10DeleteControllerFromLabel() {
		// Test controller delete in a label => supprimer de la liste
		JSONObject jo = deleteRequestJSONObject(urn_labels + "/ch1/controller?id=17", HttpResponseCodes.SC_OK);
		testController17(jo);
		
		// impossible to delete again the same controller
		deleteRequestJSONObject(urn_labels + "/ch1/17", HttpResponseCodes.SC_NOT_ACCEPTABLE);
		
		// Test impossible to delete a controller not existing in a Group
		deleteRequestJSONObject(urn_labels + "/ch1/6", HttpResponseCodes.SC_NOT_ACCEPTABLE);
	}

	
	@Test
	public void test9DeleteLabel() {
		// Test new label creation
		JSONObject label = deleteRequestJSONObject(urn_labels + "/ch6", HttpResponseCodes.SC_NO_CONTENT);
		Assert.assertNull(label);
		
		label = deleteRequestJSONObject(urn_labels + "/ch7", HttpResponseCodes.SC_NO_CONTENT);
		Assert.assertNull(label);
	}


	
	private String createJsonLabelCh6() {
		return createJsonLabel("Chambre 6", "ch6");
	}

	private void testLabelCh6(JSONObject label) {
		testLabel(label, "ch6", "Chambre 6", 0);
	}

	private String createJsonLabelCh6Bis() {
		return createJsonLabel("Chambre 6 Bis", "ch6");
	}

	private void testLabelCh6Bis(JSONObject label) {
		testLabel(label, "ch6", "Chambre 6 Bis", 0);
	}
	
	private String createJsonLabelCh7() {
		return createJsonLabel("Chambre 7", "ch7");
	}

	private void testLabelCh7(JSONObject label) {
		testLabel(label, "ch7", "Chambre 7", 0);
	}
	
	private String createJsonLabelCh7Bis() {
		return createJsonLabel("Chambre 7 Bis", "ch7");
	}

	private void testLabelCh7Bis(JSONObject label) {
		testLabel(label, "ch7", "Chambre 7 Bis", 0);
	}
	
	private void testController11(JSONObject jo) {
		testController(jo, "Chambre 11", "11", Light.class.getName(), OnOffState.Off().toString());
	}
}
